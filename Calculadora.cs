﻿using System;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Calculadora : Form

    {
        double primero, segundo;
        String operador;
        public Calculadora()

        {
            InitializeComponent();

        }

        double Sumar(double n1, double n2)

        {
            return n1 + n2;
        }

        double Restar(double n1, double n2)

        {
            return n1 - n2;
        }

        double Multiplicar(double n1, double n2)

        {
            return n1 * n2;
        }

        double Dividir(double n1, double n2)

        {
            return n1 / n2;
        }


        private void btn0_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "0";
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "9";
        }

        
        

        private void btnQ_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(txtResultado.Text)) // validar si esta vacio
            {
                // mensaje, titulo, botones, icono
                MessageBox.Show("No se puede borrar más", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                //borra solo el ultimo digito
                string texto = txtResultado.Text.Remove(txtResultado.Text.Length - 1, 1);
                txtResultado.Text = texto;
            }

        }

        private void btnD_Click(object sender, EventArgs e)
        {
            operador = "/";
            primero = double.Parse(txtResultado.Text);
            txtResultado.Clear();
        }

        private void btnM_Click(object sender, EventArgs e)
        {
            operador = "*";
            primero = double.Parse(txtResultado.Text);
            txtResultado.Clear();
        }

        private void btnR_Click(object sender, EventArgs e)
        {
            operador = "-";
            primero = double.Parse(txtResultado.Text);
            txtResultado.Clear();


        }

        private void btnS_Click(object sender, EventArgs e)
        {
            operador = "+";
            primero = double.Parse(txtResultado.Text);
            txtResultado.Clear();
        }

        private void btnI_Click(object sender, EventArgs e)
        {
            double r;
            segundo = double.Parse(txtResultado.Text);



            switch (operador)
            {
                case "+":
                    r = Sumar(primero, segundo);
                    txtResultado.Text = r.ToString();
                    break;
                case "-":
                    r = Restar(primero, segundo);
                    txtResultado.Text = r.ToString();
                    break;
                case "*":
                    r = Multiplicar(primero, segundo);
                    txtResultado.Text = r.ToString();
                    break;
                case "/":
                    if (segundo == 0)
                    {
                        MessageBox.Show("No se puede dividir entre 0", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        r = Dividir(primero, segundo);
                        txtResultado.Text = r.ToString();
                    }

                    break;
            }
        }

        private void btnSN_Click(object sender, EventArgs e)
        {
            txtResultado.Text = "-";
        }

        private void btnP_Click(object sender, EventArgs e)
        {
            if (!txtResultado.Text.Contains("."))
            {
                if (txtResultado.Text.Length < 1) // si esta vacio agregar 0.
                {
                    txtResultado.Text += "0.";
                }
                else
                {
                   
                    txtResultado.Text += ".";
                }
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bntMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;//minimizar ventana
        }

      

        private void btnB_Click(object sender, EventArgs e)
        {
            //borra todo el campo de texto
            txtResultado.Clear();
        }
    }
}
