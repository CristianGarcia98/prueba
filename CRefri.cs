﻿using System;

/* se puede poner mas de un handler por evento
 los delegados tienen un arrayList para guardar las referencias que se le asignan
 */
namespace DelegadoRefri
{
    //declarar delegados, fuera de la clase para no usar public
    delegate void DReservasBajas(int pKilos);
    delegate void DDescongelado(int pGrados);
    class CRefri
    {
       
        

        private int kilosAlimentos = 0;
        private int grados = 0;

        //variables para invocar
        private DReservasBajas delReservas;
        private DDescongelado delDescongelado;

        //constructor
        public CRefri(int pKilos, int pGrados)
        {
            //valores iniciales del refri
            kilosAlimentos = pKilos;
            grados = pGrados;
        }

        //metodos para referenciar variables
        //multicasting
        public void AdicionaMetodoReservas(DReservasBajas pMetodo)
        {
            // delReservas = pMetodo;
            delReservas += pMetodo; //agregar delegado

        }
        public void EliminaaMetodoReservas(DReservasBajas pMetodo)
        {
            delReservas -= pMetodo; //quitar delegado
        }

        public void AdicionaMetodoDescongelado(DDescongelado pMetodo)
        {
            delDescongelado += pMetodo;//agregar delegado

        }

        //propiedades necesarias
        //retorna kilos  de alimentos restantes 
        public int Kilos { get { return kilosAlimentos; } set { kilosAlimentos = value; } }
        //retorna grados del refri
        public int Grados { get { return grados; } set { grados = value; } }

        public void Trabajar(int pConsumo)
        {
            //actualizar kilos del refri
            kilosAlimentos -= pConsumo;
            //subir la temperatura
            grados += 1;

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("{0} kilos {1} grados",kilosAlimentos,grados);
            //verificar si se cumple la condicion para invocar los handlers del evento 

            //esta condicion para el evento de kilos
            if (kilosAlimentos<10)
            {
                //invocar metodo
                delReservas(kilosAlimentos);
            }
            //esta condicion para el evento de temperatura
            if (grados>0)
            {
                //invocar metodo
                delDescongelado(grados);
            }
        }
    }
}
