﻿using System;


namespace DelegadoRefri
{
    class CTienda
    {
        public static void MandaViveres(int pKilos)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-->Vamos a mandar sus víveres, estoy en la tienda");
            Console.WriteLine("Serán {0} kilos",pKilos);
        }
    }
}
