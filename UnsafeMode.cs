﻿using System;


namespace UnsafeMode
{
    class UnsafeMode
    {
        //unsafe define algo inseguro como punteros o programacion a bajo nivel
        //Puede recuperar los datos almacenados en la ubicación a la que hace referencia la variable de puntero, 
        //utilizando el método ToString ().


        unsafe static void Main(string[] args)
        {
            int ab = 32;
            int* p = &ab;
            Console.WriteLine("el valor de ab es {0}", *p);
            Console.ReadLine();


            Console.ReadKey();
        }
    }
}
