﻿using System;
using System.Windows.Forms;
using System.Threading;

namespace Simulador_Carrera_de_Carros
{
    public partial class CarreraCarros : Form
    {
        private int n1, n2, n3;

        //getter y setter como en java
        public int N1 { get { return n1; } set { n1 = value; } }
        public int N2 { get { return n2; } set { n2 = value; } }
        public int N3 { get { return n3; } set { n3 = value; } }

        Thread hilo;
        delegate void delegado(int valor);

        public CarreraCarros()
        {
            InitializeComponent();
        }


        private void Boton_Iniciar_Carrera_Click(object sender, EventArgs e)
        {
            hilo = new Thread(new ThreadStart(Auto1));
            hilo.Start();
            hilo = new Thread(new ThreadStart(Auto2));
            hilo.Start();
            hilo = new Thread(new ThreadStart(Auto3));
            hilo.Start();
            Ganador();
        }



        #region "Auto2"
        public void Auto1()
        {
            int dormir = NumeroAleatorio(1, 100);
            N1 = dormir;
            for (int i = 0; i < 101; i++)
            {
                delegado MD = new delegado(Actualizar1);
                this.Invoke(MD, new object[] { i });
                Thread.Sleep(dormir);
            }
        }

        public void Actualizar1(int valor)
        {
            p1.Value = valor;
        }

        #endregion

        #region"Auto2"
        public void Auto2()
        {
            int dormir = NumeroAleatorio(1, 100);
            N2 = dormir;
            for (int i = 0; i < 101; i++)
            {
                delegado MD = new delegado(Actualizar2);
                this.Invoke(MD, new object[] { i });
                Thread.Sleep(dormir);
            }
        }

        public void Actualizar2(int valor)
        {
            p2.Value = valor;
        }

        #endregion

        #region "Auto3"
        public void Auto3()
        {
            int dormir = NumeroAleatorio(1, 100);
            N3 = dormir;
            for (int i = 0; i < 101; i++)
            {
                delegado MD = new delegado(Actualizar3);
                this.Invoke(MD, new object[] { i });
                Thread.Sleep(dormir);
            }
        }

        public void Actualizar3(int valor)
        {
            p3.Value = valor;
        }

        #endregion

        #region "NumeroAleatorio"
        //devolver un numero aleatorio para dormir esos milisegundos el hilo
        public int NumeroAleatorio(int min, int max)
        {
            Random rand = new Random();
            return rand.Next(min, max);
        }
        #endregion

        #region "Determinar ganador"
        public void Ganador()
        {

            if (N1 != N2 && N1 != N3)
            {
                if (N1 < N2 && N1 < N3)
                {
                    //mensaje, titulo
                    MessageBox.Show("Ganó el primer auto", "Ganador");
                }
                else if (N2 < N1 && N2 < N3)
                {
                    //mensaje, titulo
                    MessageBox.Show("Ganó el segundo auto", "Ganador");
                }
                else if (N3 < N1 && N3 < N2)
                {
                    //mensaje, titulo
                    MessageBox.Show("Ganó el tercer auto", "Ganador");
                }
                else if (N1 == N2 && N1 != N3)
                {
                    //mensaje, titulo
                    MessageBox.Show("Empataron el primero auto y el segundo", "Ganador");
                }
                else if (N2 == N3 && N2 != N1)
                {
                    //mensaje, titulo
                    MessageBox.Show("Empataron el segundo auto y el tercero", "Ganador");
                }
                else if (N3 == N1 && N3 != N2)
                {
                    //mensaje, titulo
                    MessageBox.Show("Empataron el tercer auto y el primero", "Ganador");
                }

            }
            //isAlive: true si este hilo se ha iniciado y no ha terminado normalmente o abortado; de lo contrario, false.
            else if (N1 == N2 && N1 == N3 && hilo.IsAlive == false)
            {
                //mensaje, titulo
                MessageBox.Show("Empataron los 3 autos", "Empate");
            }
            #endregion
        }
    }
}
