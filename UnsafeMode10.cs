﻿using System;


namespace UnsafeMode10
{
    class UnsafeMode10
    {
        public int X;
        static void Main(string[] args)
        {
            UnsafeMode10 test = new UnsafeMode10();

            unsafe
            {
                fixed (int* p = &test.X)   // Pins test
                {
                    *p = 9;
                }
                Console.WriteLine(test.X);
            }
            Console.ReadKey();
        }
        }
    }
