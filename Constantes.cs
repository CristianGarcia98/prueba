﻿namespace Nomina
{
    internal static class Constantes
    {
       public  const double vejezP = 0.01125; //porcentaje vejez
        public const double inVidaP = 0.00625; //porcentaje invalidez y vida
        public const double prestacionesDP= 0.0025;//prestacionesDinero
        public const double prestacionesEP = 0.00375;///prestaciones en especie
        public const int aguinaldo = 15;//dias de aguinaldo minimos por ley
        public const int vacaciones = 6; //minimo de dias de vacaciones
        public const double primaV =0.25*vacaciones;//prima vacacional minima por ley
        public const double importeP = 0.004;//porcentaje de importe
    }
}
