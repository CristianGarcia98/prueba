﻿using System;


namespace UnsafeMode6
{
    class UnsafeMode6
    {
        static unsafe void Main(string[] args)
        {
            UnsafeMode6 p = new UnsafeMode6();
            int var1 = 10;
            int var2 = 20;
            int* x = &var1;
            int* y = &var2;
            Console.WriteLine("Before Swap: var1:{0}, var2: {1}", var1, var2);
            p.swap(x, y);
            Console.WriteLine("After Swap: var1:{0}, var2: {1}", var1, var2);
            Console.ReadKey();
        }

        //se pueden pasar parametros por referencia como en C/C++
        public unsafe void swap(int* p, int* q)
        {
            int temp = *p;
            *p = *q;
            *q = temp;
        }
    }
   
}
