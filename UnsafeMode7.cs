﻿using System;

/*
 En C #, un nombre de matriz y un puntero a un tipo de datos con los mismos datos de matriz no son el mismo tipo de variable. 
Por ejemplo,  

int * p e int [] p, no son del mismo tipo. Puede incrementar la variable de puntero p porque no está fija en la memoria 
pero una dirección de matriz está fija en la memoria y
no puede incrementar eso.
 
Este ejemplo imprime la direccion de memoria y el valor guardado en esta
 */
namespace UnsafeMode7
{
    class UnsafeMode7
    {
        static  unsafe void Main(string[] args)
        {
            int[] list = { 10, 20, 30, 40, 50 };
            fixed (int* ptr = list)
                for (int i = 0; i < list.Length; i++)
                {
                    Console.WriteLine("Address of list[{0}]={1}", i, (int)(ptr + i));
                    Console.WriteLine("Value of list[{0}]={1}", i, *(ptr + i));
                }
            Console.ReadKey();
        }
    }
}
