﻿using System;


namespace UnsafeMode2
{
    class UnsafeMode2
    {

        //fixed: localidad de memoria fija
        //unsafe: codigo inseguro como punteros
        unsafe static void Main()
        {
            fixed (char* value = "safe")
            {
                char* ptr = value;
                while (*ptr != '\0')
                {
                    Console.WriteLine(*ptr);
                    ++ptr;
                }
            }

            Console.ReadKey();//evitar que se cierre la consola
        }
    }
}
