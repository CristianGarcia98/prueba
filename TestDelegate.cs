﻿using System;

delegate int NumberChanger(int n);

/*
 Los delegados de C # son similares a los punteros a las funciones, en C o C ++. Un delegado es una variable de tipo de referencia que contiene 
la referencia a un método. La referencia se puede cambiar en tiempo de ejecución.
Los delegados se utilizan especialmente para implementar eventos y los métodos de devolución de llamada.
 delegate <return type> <delegate-name> <parameter list>
 */
namespace TestDelegate
{
    class TestDelegate
    {
        static int num = 10;
        public static int AddNum(int p)
        {
            num += p;
            return num;
        }
        public static int MultNum(int q)
        {
            num *=q;
            return num;
        }

        public static int getNum()
        {
            return num;
        }



        static void Main(string[] args)
        {
            NumberChanger nc1 = new NumberChanger(AddNum); //referencia a la función AddNum
            NumberChanger nc2 = new NumberChanger(MultNum); //referencia a la función MultNum
            nc1(25);
            Console.WriteLine("Value of num: {0}",getNum());
            nc2(5);
            Console.WriteLine("Value of num: {0}", getNum());
            Console.ReadKey();

        }
    }
}
