﻿using System.Collections.Generic;
using System.IO;


namespace TutoriaLINQ2
{
    public static class Extensions
    {
        /*
         * Cuando utiliza la  palabra clave contextual yield en una declaración, indica que el método, 
         * operador o getdescriptor de acceso en el que aparece es un iterador. El uso yieldpara definir un iterador 
         * elimina la necesidad de una clase extra explícita 
         * (la clase que contiene el estado de una enumeración).
         */

        /*
         * Puede ver la incorporación del modificador this del primer argumento al método. Esto significa que se llama al método
         * como si fuese un método de miembro del tipo del primer argumento. Esta declaración de método también 
         * sigue una expresión estándar donde los tipos de entrada y salida son IEnumerable<T>. Dicha práctica permite 
         * que los métodos LINQ 
         * se encadenen entre sí para realizar consultas más complejas.
         */
        public static IEnumerable<T> InterleaveSequenceWith<T>
       (this IEnumerable<T> first, IEnumerable<T> second)
        {
            var firstIter = first.GetEnumerator();
            var secondIter = second.GetEnumerator();

            while (firstIter.MoveNext() && secondIter.MoveNext())
            {
                yield return firstIter.Current;//obtener elemento en la posicion actual del enumerador
                yield return secondIter.Current;
            }
        }

        public static bool SequenceEquals<T>
    (this IEnumerable<T> first, IEnumerable<T> second)
        {
            var firstIter = first.GetEnumerator();
            var secondIter = second.GetEnumerator();

            while (firstIter.MoveNext() && secondIter.MoveNext())
            {
                if (!firstIter.Current.Equals(secondIter.Current))
                {
                    return false;
                }
            }

            return true;
        }

        public static IEnumerable<T> LogQuery<T>
    (this IEnumerable<T> sequence, string tag)
        {
            // File.AppendText creates a new file if the file doesn't exist.
            using (var writer = File.AppendText("debug.log"))
            {
                writer.WriteLine($"Executing Query {tag}");
            }

            return sequence;
        }
    }
}
