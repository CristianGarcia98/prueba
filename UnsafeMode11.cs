﻿using System;


namespace UnsafeMode11
{
    //se puede acceder al miembro de una estructura con -> igual que en C/C++
    struct Test
    {
        public int X;
    }
    class UnsafeMode11
    {
        unsafe static void Main(string[] args)
        {
            Test test = new Test(); // se puede instanciar una estructura en c#
            Test* p = &test;//puntero a estructura
            p->X = 9;//asignar valor a X con el puntero
            Console.WriteLine(test.X);
            Console.ReadKey();
        }
    }
}
