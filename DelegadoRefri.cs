﻿using System;


namespace DelegadoRefri
{
    //comunicacion del main al delegado
    class DelegadoRefri
    {
        static void Main(string[] args)
        {
            //Crear refri
            CRefri miRefri = new CRefri(70,-20);
            Random rnd = new Random();

            //adicionar handlers o manejadores de eventos
            //  miRefri.AdicionaMetodoReservas(new CRefri.DReservasBajas(InformeKilos));
            //miRefri.AdicionaMetodoDescongelado(new CRefri.DDescongelado(InformeGrados));
            DReservasBajas kilos1 = new DReservasBajas(InformeKilos);
            DReservasBajas kilos2 = new DReservasBajas(CTienda.MandaViveres);
            DDescongelado desc1 = new DDescongelado(InformeGrados);


            //añadir handlers 
            miRefri.AdicionaMetodoReservas(kilos1);
            miRefri.AdicionaMetodoReservas(kilos2);
            miRefri.AdicionaMetodoDescongelado(desc1);

            //el refri trabaja
            while (miRefri.Kilos>0)
            {
                miRefri.Trabajar(rnd.Next(1,5));
            }
            miRefri.EliminaaMetodoReservas(kilos2);///quitar referencias

           
            miRefri.Kilos = 50;
            miRefri.Grados = -15;

            while (miRefri.Kilos > 0)
            {
                miRefri.Trabajar(rnd.Next(1, 5));
            }


            Console.ReadKey();//evitar que se cierre la consola
        }
        public static void InformeKilos(int pKilos)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("-->Reservas bajas de alimentos, estoy a nivel de main");
            Console.WriteLine("-->Quedan {0} kilos", pKilos);
        }

        public static void InformeGrados(int pGrados)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("-->Se descongela el refri!, estoy a nivel de main");
            Console.WriteLine("-->Esta a {0} grados", pGrados);
        }
    }
}
