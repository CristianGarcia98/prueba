﻿using System;
using System.Windows.Forms;

namespace Nomina
{
    public partial class Nomina : Form
    {
        public Nomina()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            double dias = Convert.ToDouble(txtDias.Text);
            double salarioD = Convert.ToDouble(txtDiario.Text);
            double totalPercepciones = dias * salarioD;
            lblImporte.Text = totalPercepciones.ToString();
            
            double factorIntegracion = 365+Constantes.primaV+ Constantes.aguinaldo + Constantes.vacaciones;
            factorIntegracion = factorIntegracion/ 365;
            double salarioDI = factorIntegracion * salarioD;
            double ISR, excedente, cuotaFija, ISRTotal,sueldoTotal,UMA,diferencia,importe2,totalImss,prestacionesD;
            double prestacionesE,inVida,vejez,totalDeducciones,subsidioEm;

            lblSDI.Text = salarioDI.ToString();
            bool subsidio=false;
            UMA = Convert.ToDouble(lblUMA.Text);
          

            if (salarioD*30 <6000)
            {
                subsidio = true;
            }
            else
            {
                subsidio = false;
                subsidioEm = 0;
                lblSub.Text = subsidioEm.ToString();
            }
          

           

            if (salarioDI>=0.01 || salarioDI <=578.52)
            {
                excedente = salarioDI - 0.01;
                excedente *= 0.0192; // 1.92%
                cuotaFija = 0;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salarioDI - ISR;
                lblISR.Text = ISRTotal.ToString();

                if ((UMA * 3) < salarioDI)
                {
                    diferencia = salarioDI - (UMA * 3);
                    importe2 = (diferencia * dias) * Constantes.importeP;//0.40%
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio==true)
                    {
                        
                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 && totalPercepciones<1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones>= 1309.21 && totalPercepciones<1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones>=1713.61 && totalPercepciones<1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones>=1745.71 && totalPercepciones<2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones>=2193.76 && totalPercepciones<2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones>=2327.56 && totalPercepciones<2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones>=2632.66 && totalPercepciones<3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones>=3071.41 && totalPercepciones<3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones>=3510.16 && totalPercepciones<3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones>=3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio==false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }



                }
                else if ((UMA * 3) > salarioDI)
                {
                    diferencia = 0;
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();
                }

            }
            else if (salarioDI>=578.53 || salarioDI <=4910.18)
            {
                excedente = salarioDI - 578.53;
                excedente *= 0.064;//6.40%
                cuotaFija = 11.11;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salarioDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salarioDI)
                {
                    diferencia = salarioDI - (UMA * 3);
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio==false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salarioDI)
                {
                    diferencia = 0;
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
            else if (salarioDI>=4910.19 || salarioDI <=8629.20)
            {
                excedente = salarioDI - 4910.19;
                excedente *= 0.1088;//10.88%
                cuotaFija = 288.33;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salarioDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salarioDI)
                {
                    diferencia = salarioDI - (UMA * 3);
                    importe2 = (diferencia * dias) * Constantes.importeP;//0.0040%
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salarioDI)
                {
                    diferencia = 0;
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();
                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }

           
            else if (salarioDI>=8629.21 || salarioDI <=10031.07)
            {
                excedente = salarioDI - 8629.21;
                excedente *= 0.16;//%16;
                cuotaFija = 692.96;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salarioDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salarioDI)
                {
                    diferencia = salarioDI - (UMA * 3);
                    importe2 = (diferencia * dias) * Constantes.importeP;//0.0040%
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salarioDI)
                {
                    diferencia = 0;
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {

                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }

                }
            }
            else if (salarioDI>=10031.08 || salarioDI <=12009.94)
            {
                excedente = salarioDI - 10031.08;
                excedente *= 0.1792;//17.92%
                cuotaFija = 917.26;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salarioDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salarioDI)
                {
                    diferencia = salarioDI - (UMA * 3);
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salarioDI)
                {
                    diferencia = 0;
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();
                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }

            }
            else if (salarioDI>= 12009.95 || salarioDI <=24222.31)
            {
                excedente = salarioDI - 12009.95;
                excedente *= 0.2126;//21.36%
                cuotaFija = 1271.87;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salarioDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salarioDI)
                {
                    diferencia = salarioDI - (UMA * 3);
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salarioDI)
                {
                    diferencia = 0;
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();
                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
            else if (salarioDI>= 24222.32 || salarioDI <=38177.69)
            {
                excedente = salarioDI - 24222.32;
                excedente *= 0.2352;//23.52%
                cuotaFija=3880.44;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salarioDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salarioDI)
                {
                    diferencia = salarioDI - (UMA * 3);
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salarioDI)
                {
                    diferencia = 0;
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();
                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
            else if (salarioDI>= 38177.70 || salarioDI <=72887.50)
            {
                excedente = salarioDI - 38177.70;
                excedente *= 0.3;//3%
                cuotaFija = 7162.74;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salarioDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salarioDI)
                {
                    diferencia = salarioDI - (UMA * 3);
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }

                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salarioDI)
                {
                    diferencia = 0;
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();


                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }

                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
            else if (salarioDI>=72887.51 || salarioDI <=97183.33)
            {
                excedente = salarioDI - 72887.51;
                excedente *= 0.32;//32%
                cuotaFija = 17575.69;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salarioDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salarioDI)
                {
                    diferencia = salarioDI - (UMA * 3);
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salarioDI)
                {
                    diferencia = 0;
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
            else if (salarioDI>= 97183.34 || salarioDI <=291550)
            {
                excedente = salarioDI - 97183.34;
                excedente *= 0.34;//34%
                cuotaFija = 25350.35;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salarioDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salarioDI)
                {
                    diferencia = salarioDI - (UMA * 3);
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salarioDI)
                {
                    diferencia = 0;
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }


                }
            }
            else if (salarioDI>=291550.01 || salarioDI <=99999999.99)
            {
                excedente = salarioDI - 291550.01;
                excedente *= 0.35;//35%
                cuotaFija = 91435.02;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salarioDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salarioDI)
                {
                    diferencia = salarioDI - (UMA * 3);
                    importe2 = (diferencia * dias) * Constantes.importeP;//0.0040%
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salarioDI)
                {
                    diferencia = 0;
                    importe2 = (diferencia * dias) * Constantes.importeP;
                    prestacionesD = salarioDI * dias * Constantes.prestacionesDP;
                    prestacionesE = salarioDI * dias * Constantes.prestacionesEP;
                    inVida = salarioDI * dias * Constantes.inVidaP;
                    vejez = salarioDI * dias * Constantes.vejezP;
                    totalImss = diferencia + importe2 + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = totalImss.ToString();
                    totalDeducciones = ISRTotal + totalImss;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
        }

        private void txtDiario_KeyPress(object sender, KeyPressEventArgs v)
        {
            if (Char.IsDigit(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (Char.IsSeparator(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (Char.IsControl(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (v.KeyChar.ToString().Equals("."))
            {
                v.Handled = false;
            }
            else
            {
                v.Handled = true;
                MessageBox.Show("Solo numeros o numeros con punto decimal");
            }
        }

        private void txtDias_KeyPress(object sender, KeyPressEventArgs v)
        {
            if (Char.IsDigit(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (Char.IsSeparator(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (Char.IsControl(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (v.KeyChar.ToString().Equals("."))
            {
                v.Handled = false;
            }
            else
            {
                v.Handled = true;
                MessageBox.Show("Solo numeros o numeros con punto decimal");
            }
        }
    }
}