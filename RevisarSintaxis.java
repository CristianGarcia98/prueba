package revisarsintaxis;
import java.util.Scanner;
import java.util.Stack;
 
public class RevisarSintaxis {
    
static char[] open = {'(','{','['};
static char [] close = {')','}',']'};

static boolean isOpen(char c){
    for (int i = 0; i < open.length; i++) 
        if (open[i]==c) 
            return true;
    
    return false;
}

//retorna el simbolo inverso al parametro que recibe, por defecto un espacio en blanco
static char getInv(char c){
    for (int i = 0; i < open.length; i++) 
        if (open[i]==c) 
            return close[i];
    
    
    for (int i = 0; i < close.length; i++) 
        if (close[i]==c) 
            return open[i];
    
    return ' ';
}

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String expression = input.next();
        char[] expr = expression.toCharArray();
        Stack<Character> s = new Stack<>();
        boolean correct = true;
        for (int i = 0; i < expr.length; i++) {
            if (isOpen(expr[i])) 
                s.push(expr[i]);
            else {
                if (s.isEmpty()) {
                    correct = false;
                    break;
                }else if (s.peek()!= getInv(expr[i])) {
                    correct = false;
                    break;
                }else
                    s.pop();
            } 
    }
        if (!s.empty()) 
            correct = false;
            
      if(correct)
            System.out.println("La expresión "+expression +" es correcta");
        else 
         System.err.println("La expresión "+expression+" es incorrecta");
            
        
    }
    
}
