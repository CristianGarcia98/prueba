package sockets2;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.io.BufferedReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class EchoServer {
    private ServerSocket serverSocket;
     private PrintWriter out;
    private BufferedReader in;
    private Socket clientSocket;
     
    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        clientSocket = serverSocket.accept();      
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        
         String inputLine;
        while ((inputLine = in.readLine()) != null) {
        if (".".equals(inputLine)) {
            out.println("good bye");
            break;
         }
        }
         out.println(inputLine);
        }
    
    public void stop(){
        try {
            in.close();
            out.close();
            clientSocket.close();
            serverSocket.close();
        } catch (IOException ex) {
           Logger.getLogger(EchoServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) throws IOException {
         EchoServer server = new EchoServer();
        server.start(4444);
    }
    }