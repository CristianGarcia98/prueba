import java.io.*;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

public class Main {

    static double eps=(double)1e-7;
    static long mod=(int)1e9+7;
    public static void main(String args[]){
        InputReader in = new InputReader(System.in);
        OutputStream outputStream = System.out;
        PrintWriter out = new PrintWriter(outputStream);
        
        int n1=in.nextInt();
        int n2=in.nextInt();
        int n3=in.nextInt();
        long s1=0,s2=0,s3=0;
        Queue<Integer> st1=new LinkedList<Integer>();
        Queue<Integer> st2=new LinkedList<Integer>();
        Queue<Integer> st3=new LinkedList<Integer>();
        
        for(int i=0;i<n1;i++){
            int x=in.nextInt();
            st1.add(x);
            s1+=x;
        }
        for(int i=0;i<n2;i++){
            int x=in.nextInt();
            st2.add(x);
            s2+=x;
        }
        for(int i=0;i<n3;i++){
            int x=in.nextInt();
            st3.add(x);
            s3+=x;
        }
        while(!(s1==s2 && s2==s3)){
            long max=Math.max(s1, Math.max(s2, s3));
            if(s1==max){
                s1-=st1.poll();
            }
            else if(s2==max){
                s2-=st2.poll();
            }
            else{
                s3-=st3.poll();
            }
        }
        out.println(s1);
        out.close();
        

    }

    static class Pair implements Comparable<Pair> {
        long u;
        int v;
        int index=-1;
        public Pair(long u, int v) {
            this.u = u;
            this.v = v;
        }

        public int hashCode() {
            int hu = (int) (u ^ (u >>> 32));
            int hv = (int) (v ^ (v >>> 32));
            return 31 * hu + hv;
        }

        public boolean equals(Object o) {
            Pair other = (Pair) o;
            return u == other.u && v == other.v;
        }

        public int compareTo(Pair other) {
            return Long.compare(u, other.u) != 0 ? Long.compare(u, other.u) : Long.compare(v, other.v);
        }

        public String toString() {
            return "[u=" + u + ", v=" + v + "]";
        }
    }
    public static void debug(Object... o) {
        System.out.println(Arrays.deepToString(o));
    }
    static long modulo(long a,long b,long c) {
        long x=1;
        long y=a;
        while(b > 0){
            if(b%2 == 1){
                x=(x*y)%c;
            }
            y = (y*y)%c; 
            b /= 2;
        }
        return  x%c;
    }
    static long gcd(long x, long y)
    {
        if(x==0)
            return y;
        if(y==0)
            return x;
        long r=0, a, b;
        a = (x > y) ? x : y; 
        b = (x < y) ? x : y; 
        while(a % b != 0)
        {
            r = a % b;
            a = b;
            b = r;
        }
        return r;
    }
    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream inputstream) {
            reader = new BufferedReader(new InputStreamReader(inputstream));
            tokenizer = null;
        }

        public String nextLine(){
            String fullLine=null;
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    fullLine=reader.readLine();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                return fullLine;
            }
            return fullLine;
        }
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
        public long nextLong() {
            return Long.parseLong(next());
        }
        public int nextInt() {
            return Integer.parseInt(next());
        }
    }
}