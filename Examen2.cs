﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Examen2
{
    class Examen2
    {
        private static Semaphore pooldechalanes;
        private static int _padding;
        private static int n = 0;
        private static int m = 0;
        private static int o = 0;
        private static int c = 0;
        private static void ChalanAlbanil(object chalan)
        {
            Console.WriteLine("Chalan de Albañil");
            Console.WriteLine("Thread {0}: {1}, Priority {2}",
                        Thread.CurrentThread.ManagedThreadId,
                        Thread.CurrentThread.ThreadState,
                        Thread.CurrentThread.Priority);
            Console.WriteLine("Chalan {0} esperando turno...", chalan);
            pooldechalanes.WaitOne();
            // A padding interval to make the output more orderly.
            int padding = Interlocked.Add(ref _padding, 100);
            Console.WriteLine("Chalan {0} obtiene turno...", chalan);
            Thread.Sleep(1000 + padding);
            Console.WriteLine("Chalan {0} termina turno...", chalan);
            Console.WriteLine("Chalan {0} libera su lugar {1}",
                chalan, pooldechalanes.Release());
        }
        private static  void ConstruirCasas(int cantidad)
        {
            for (int i = 1; i <=cantidad; i++)
            {
                Console.WriteLine("Construyendo casa {0}",i);
            }
        }

        private static  void ConstruirBanos(int[] cantidad)
        {
            for (int i = 1; i <= cantidad.Length; i++)
            {
                Console.WriteLine("Construyendo baño {0}",i);
            }
        }
        private static void ConstruirHab(int[] cantidad)
        {
            for (int  i= 1;  i<=cantidad.Length; i++)
            {
                Console.WriteLine("Construyendo habitación {0}", i);
            }
        }

        public static async Task Main(string[] args)
        {
            Console.WriteLine("¿Cuántos chalanes llegaron a chambear?");
            n = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos pueden trabajar?");
            m = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿Cuántos segundos hay para contruir?");
            o = Convert.ToInt32(Console.ReadLine()) * 1000;
            Console.WriteLine("¿Cuántas casas se van a construir?");
            c = Convert.ToInt32(Console.ReadLine());
            int[] habArr = new int[c];
            int[] banosArr = new int[c];
            for (int i = 0; i < habArr.Length; i++)
            {
                Console.WriteLine("¿Cuántas habitaciones se van a construir en la casa {0}?",i);
                habArr[i] = Convert.ToInt32(Console.ReadLine());
            }
            for (int i = 0; i < banosArr.Length; i++)
            {
                Console.WriteLine("¿Cuántos baños se  van a construir en la casa {0}?",i);
                banosArr[i] = Convert.ToInt32(Console.ReadLine());
            }
       
            
            pooldechalanes = new Semaphore(0, m);

            Console.WriteLine("Inicia la construcción");

            for (int i = 1; i <= n; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(ChalanAlbanil));
                if (i % 2 == 0)
                {
                    t.Priority = ThreadPriority.AboveNormal;
                }
                else
                {
                    t.Priority = ThreadPriority.BelowNormal;
                }
                t.Start(i);
            }

            Thread.Sleep(500);
            Console.WriteLine("Se libera un turno");
            pooldechalanes.Release(m);

            Thread.Sleep(o);
            Task casas =  Task.Run(() => { ConstruirCasas(c); });
            Task banos =  Task.Run(() => { ConstruirBanos(banosArr); });
            Task cuartos = Task.Run(() => { ConstruirHab(habArr); });

            var ConstruirTask = new List<Task> { casas,banos,cuartos};
            while (ConstruirTask.Count>0)
            {
                Task finishedTask = await Task.WhenAny(ConstruirTask);

                if (finishedTask==casas)
                {
                    Console.WriteLine("Casas terminadas");
                }
                else if (finishedTask==banos)
                {
                    Console.WriteLine("Baños terminados");

                }
                else if (finishedTask==cuartos)
                {
                    Console.WriteLine("Cuartos terminados");
                }
                ConstruirTask.Remove(finishedTask);
            }
            Console.WriteLine("Se terminó la construcción");
            Console.ReadKey();
            

        }
    }
}