﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TaskBasedAsynchronousProgramming
{
    //ejemplo de programacion asincrona usando Task
    //$ hace un string fijo
    class TaskBasedAsynchronousProgramming
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Main Thread : {Thread.CurrentThread.ManagedThreadId} Statred");
            Task task1 = new Task(Impresiones);
            task1.Start();
            Console.WriteLine($"Main Thread : {Thread.CurrentThread.ManagedThreadId} Completed");
            Task task2 = Task.Factory.StartNew(Nomina);// crea e inicia automaticamente la tarea
           // task2.Start();
            Task task3 = Task.Run(() => { Pagos(); });//expresion lambda, Run pone en cola la tarea y la devuelve 
            //factory es el patron de diseño  fabrica que ya vi en java
            //task3.Start();

            Console.ReadKey();
        }
        /*
        static void PrintCounter()
        {
            //obtener identificador unico del subproceso: ManagedThreadId
            //subproceso o hilo actual: CurrentThread
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Started");
            for (int count = 1; count <= 5; count++)
            {
                Console.WriteLine($"count value: {count}");
            }
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Completed");
        }*/
        static void Nomina()
        {
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Started");
            Console.WriteLine("Inicio nomina");
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine("Procesando nomina {0}",i);
            }
            Console.WriteLine("Fin nomina");
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Completed");
        }
        static void Pagos()
        {
            Console.WriteLine("Inicio Pagos");
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine("Procesando pago {0}", i);
            }
            Console.WriteLine("Fin Pagos");
        }

        static void Impresiones()
        {
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Completed");
            Console.WriteLine("Inicio Impresiones");
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine("Procesando impresiones {0}", i);
            }
            Console.WriteLine("Fin impresiones");
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Completed");
        }
    }
    }
