package sockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GreetServer {
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
 
    
     public void start(int port) throws IOException  {
         try{
        serverSocket = new ServerSocket(port);
        clientSocket = serverSocket.accept();
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        String greeting = in.readLine();
            if ("hello server".equals(greeting)) 
                out.println("hello client");
            
            else 
                out.println("unrecognised greeting");
            
         }catch(IOException ex){
              Logger.getLogger(GreetServer.class.getName()).log(Level.SEVERE, null, ex);
         }
             
    }
     
     public void stop()   {
            try {
          
            in.close();
            out.close();
            clientSocket.close();
            serverSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(GreetServer.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
     
    
    
    public static void main(String[] args) throws IOException {
            GreetServer server=new GreetServer();
            server.start(6666);
        }
    }
