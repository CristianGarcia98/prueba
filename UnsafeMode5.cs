﻿using System;


//unsafe puede asignarse a funciones tambien

namespace UnsafeMode5
{
    class UnsafeMode5
    {
        static unsafe void Main(string[] args)
        {
            Unsafe ();
            Console.ReadKey();
        }

        public static unsafe void Unsafe()
        {
            int var = 20;
            int* p = &var;
            Console.WriteLine("Data is: {0} ", var);
            Console.WriteLine("Data is: {0} ", p->ToString());
            Console.WriteLine("Address is: {0} ", (int)p);
        }
    }
}
